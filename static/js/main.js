function init(refPoints) {
    var multiRoute = new ymaps.multiRouter.MultiRoute({
        referencePoints: refPoints,
        params: {
            //Тип маршрутизации - пешеходная маршрутизация.
            routingMode: 'pedestrian'
        }
    }, {
        // Автоматически устанавливать границы карты так, чтобы маршрут был виден целиком.
        boundsAutoApply: true
    });


    // Создаем карту с добавленной на нее кнопкой.
    var myMap = new ymaps.Map('map', {
        center: [55.739625, 37.54120],
        zoom: 12,
    }, {
        buttonMaxWidth: 300
    });

    // Добавляем мультимаршрут на карту.
    myMap.geoObjects.add(multiRoute);
}

function errLoadRoute() {

}

function randRoute(data) {
    let letters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'

    let parent = document.querySelector('.route-viewer'),
        divMap = document.createElement("div");
    divMap.setAttribute('id', 'map');

    // Clear items
    parent.innerHTML = '';

    // Add map block
    parent.appendChild(divMap);

    let elPoints = document.createElement("div")
    elPoints.classList.add('route-viewer__points');

    elRating = document.querySelector('.rating')
    elRating.removeAttribute('class');
    elRating.classList.add('rating');

    let refPoints = [];
    data.Points.forEach(function (point, idx) {
        // Coordinats for map
        refPoints.push([point.Latitude, point.Longitude])

        let elPoint = document.createElement("div"),
            elPointLetter = document.createElement("div"),
            elPointTitle = document.createElement("h2"),
            elPointImage = document.createElement("img"),
            elPointInfo = document.createElement("div"),
            elPointDescription = document.createElement("p"),
            elPointRating = document.createElement("div");

        elPoint.classList.add('route-point');
        elPointLetter.innerText = letters.charAt(idx);
        elPointLetter.classList.add('route-point__letter');
        elPointTitle.innerText = point.Title;
        elPointTitle.classList.add('route-point__title');
        elPointImage.setAttribute('src', point.ImageURL);
        elPointImage.classList.add('route-point__image');
        elPointInfo.classList.add('route-point__info');
        elPointDescription.innerText = point.Description;
        elPointDescription.classList.add('route-point__description');

        elRate = elRating.cloneNode(true);
        elRate.classList.add('rating_' + Math.round(point.Rating));
        elPointRating.classList.add('route-point__rating');

        elPoint.appendChild(elPointLetter);
        elPoint.appendChild(elPointImage);
        elPoint.appendChild(elPointInfo);
        elPointInfo.appendChild(elPointTitle);
        elPointRating.appendChild(elRate)
        elPointInfo.appendChild(elPointRating);
        elPointInfo.appendChild(elPointDescription);
        elPoints.appendChild(elPoint)
    });

    ymaps.ready(init(refPoints));

    let elTitle = document.createElement("h1");
    elTitle.classList.add('route-viewer__title');
    elTitle.innerText = data.Title

    let elComments = document.createElement('div');
    elComments.classList.add('route-viewer__comments');
    elComments.classList.add('route-comments');


    let elCommentsTitle = document.createElement('h3');
    elCommentsTitle.classList.add('route-comments__title');
    elCommentsTitle.innerText = 'Комментарии';

    let elCommentsSection = document.createElement('div');
    elCommentsSection.classList.add('route-comments__items')

    data.Comments.forEach(function (comment) {
        let elComment = document.createElement('div'),
            elCommentUser = document.createElement("div"),
            elCommentUserName = document.createElement("div"),
            elCommentUserAvatar = document.createElement("img"),
            elCommentUserLvl = document.createElement("div"),
            elCommentSection = document.createElement("div"),
            elCommentRating = document.createElement("div"),
            elCommentText = document.createElement("p");

        elComment.classList.add('route-comment');
        elCommentUser.classList.add('route-comment__user');
        elCommentUserName.classList.add('route-comment__user-name');
        elCommentUserAvatar.classList.add('route-comment__user-avatar');
        elCommentUserLvl.classList.add('route-comment__user-lvl');
        elCommentRating.classList.add('route-comment__rating');
        elCommentSection.classList.add('route-comment__section');
        elCommentText.classList.add('route-comment__text');

        elCommentUserName.innerText = comment.User.Name;
        elCommentUserAvatar.setAttribute('src', comment.User.AvatarURL);
        elCommentUserLvl.innerText = '(' + comment.User.Level + 'lvl)';
        elCommentText.innerText = comment.Text

        elRate = elRating.cloneNode(true);
        elRate.classList.add('rating_' + Math.round(comment.Rating));
        elRate.classList.add('rating_big');
        elCommentRating.classList.add('route-comment__rating');
        elCommentRating.appendChild(elRate)

        elCommentUser.appendChild(elCommentUserName);
        elCommentUser.appendChild(elCommentUserLvl);
        elCommentUser.appendChild(elCommentUserAvatar);
        elCommentSection.appendChild(elCommentRating);
        elCommentSection.appendChild(elCommentText);
        elComment.appendChild(elCommentUser);
        elComment.appendChild(elCommentSection);
        elCommentsSection.appendChild(elComment);
    })

    elComments.appendChild(elCommentsTitle);
    elComments.appendChild(elCommentsSection);

    // Add title
    parent.appendChild(elTitle);
    // Add points
    parent.appendChild(elPoints);
    // Add comments
    parent.appendChild(elComments);
}

function loadRoute(id) {
    let request = new XMLHttpRequest();
    request.open('GET', '/api/getRoute?id=' + id, true);

    request.onload = function () {
        if (request.status >= 200 && request.status < 400) {
            var data = JSON.parse(request.responseText);
            randRoute(data)
        } else {
            errLoadRoute()
        }
    };

    request.onerror = function () {
        errLoadRoute()
    };

    request.send();
}

var data = new Array;

function removeActivity() {
    this.parentNode.removeChild(this);
}

function addActivity() {
    if (this.value == 'none') {
        return
    }

    if (data[this.parentNode.dataset.date] == undefined) {
        data[this.parentNode.dataset.date] = new Array;
    }

    data[this.parentNode.dataset.date].push(this.value);


    let elActivity = this.parentNode.querySelector('.vocation-pipeline__date-activity'),
        activityItem = document.createElement('div');

    activityItem.classList.add('activity');
    activityItem.classList.add('activity_' + this.value);
    activityItem.addEventListener('click', removeActivity)

    elActivity.appendChild(activityItem);
}


function vocationPipeline() {
    let beginningVocation = document.getElementById('beginningVocation').value,
        endVocation = document.getElementById('endVocation').value;

    if (beginningVocation == '' || endVocation == '') {
        return
    }

    var start = new Date(beginningVocation);
    var end = new Date(endVocation);

    let parent = document.querySelector('.vocation-pipeline'),
        dateOptions = {
            weekday: 'short',
            month: 'numeric',
            day: 'numeric'
        },
        activityOptions = ['none', 'bicycle', 'boat', 'car', 'fly', 'hiking', 'muslim', 'orthodox', 'run', 'walk', ],
        activityOptionsText = {
            'none': 'Ничего',
            'bicycle': 'На велосипеде',
            'boat': 'По воде',
            'car': 'На машине',
            'fly': 'По воздуху',
            'hiking': 'По горам',
            'muslim': 'Мечети',
            'orthodox': 'Церкви',
            'run': 'Пробежка',
            'walk': 'Прогулка',
        };

    var loop = new Date(start);
    while (loop <= end) {
        var newDate = loop.setDate(loop.getDate() + 1);
        loop = new Date(newDate);

        var elDate = document.createElement("div"),
            elDateText = document.createElement("div"),
            elDateActivity = document.createElement("div"),
            elDateActivitySelect = document.createElement("select");

        activityOptions.forEach(function (activity) {
            elActivity = document.createElement("option");
            elActivity.setAttribute('value', activity);
            elActivity.innerText = activityOptionsText[activity];
            elDateActivitySelect.appendChild(elActivity);
        })

        elDate.classList.add('vocation-pipeline__date')
        elDateText.classList.add('vocation-pipeline__date-text')
        elDateActivity.classList.add('vocation-pipeline__date-activity')
        elDateActivitySelect.classList.add('vocation-pipeline__date-activity-select')

        elDateActivitySelect.addEventListener('change', addActivity);

        elDateText.innerText = loop.toLocaleDateString('ru-RU', dateOptions);

        elDate.dataset.date = loop.toJSON();

        elDate.appendChild(elDateText);
        elDate.appendChild(elDateActivity);
        elDate.appendChild(elDateActivitySelect);

        parent.appendChild(elDate);
    }



    let elFilters = document.querySelector('.vocation-filters'),
        elChildLabel = document.createElement('label');

    elChildLabel.innerText = 'С детьми'

    elChild = document.createElement('input')
    elChild.setAttribute('type', 'checkbox');
    elChildLabel.appendChild(elChild);
    elFilters.appendChild(elChildLabel);

    elLink = document.createElement('a');

    let elInputs = document.querySelector('.vocation-inputs');
    elSubmit = document.createElement('input');
    elSubmit.setAttribute('type', 'submit');
    elSubmit.setAttribute('value', 'Отправить');
    elSubmit.classList.add('trip-submit')

    elSubmit.addEventListener('click', function () {
        window.location.href = '/route';
    })

    elInputs.appendChild(elSubmit)
}
