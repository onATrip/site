function errLogin(data) {
    if (data != null && data != '') {
        alert(data.error);
    }
}

function makeLogin() {
    let username = document.querySelector('#loginUsername').value,
        password = document.querySelector('#loginPassword').value;

    let request = new XMLHttpRequest();
    request.open('POST', '/api/login', true);
    request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
    request.send('username=' + username + '&password=' + password);

    request.onload = function () {
        if (request.responseText == null || request.responseText == '') {
            errLogin()
        }

        var data = JSON.parse(request.responseText);
        if (request.status >= 200 && request.status < 400) {
            if (data.success == true) {
                window.location.href = '/';
            }
        } else {
            errLogin(data)
        }
    };

    request.onerror = function () {
        errLogin()
    };

    request.send();
}

function errSignup(data) {
    if (data != null && data != '') {
        alert(data.error);
    }
}

function makeSingup() {
    let name = document.querySelector('#signupName').value,
        avatarURL = document.querySelector('#signupAvatarURL').value,
        username = document.querySelector('#signupUsername').value,
        password = document.querySelector('#signupPassword').value;

    let request = new XMLHttpRequest();
    request.open('POST', '/api/signup', true);
    request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
    request.send('name=' + name + '&avatarURL=' + avatarURL + '&username=' + username + '&password=' + password);

    request.onload = function () {
        if (request.responseText == null || request.responseText == '') {
            errSignup()
        }

        var data = JSON.parse(request.responseText);
        if (request.status >= 200 && request.status < 400) {
            if (data.success == true) {
                window.location.href = '/';
            }
        } else {
            errSignup(data)
        }
    };

    request.onerror = function () {
        errSignup()
    };

    request.send();

}
