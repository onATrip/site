package model

import "time"

type Point struct {
	ID          uint `gorm:"primary_key"`
	CreatedAt   time.Time
	UpdatedAt   time.Time  `json:"-"`
	DeletedAt   *time.Time `sql:"index" json:"-"`
	RouteID     uint64
	Title       string
	Latitude    float64
	Longitude   float64
	Description string
	Rating      float64
	ImageURL    string
}
