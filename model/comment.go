package model

import "time"

type Comment struct {
	ID        uint `gorm:"primary_key"`
	CreatedAt time.Time
	UpdatedAt time.Time  `json:"-"`
	DeletedAt *time.Time `sql:"index" json:"-"`
	RouteID   uint64     `sql:"index" json:"-"`
	UserID  uint64     `sql:"index" json:"-"`
	User    User
	Rating    int
	Text      string
}
