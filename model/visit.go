package model

import "time"

// Visit is a table field for logging visits
type Visit struct {
	ID         uint64 `sql:"primary_key"`
	CreatedAt  time.Time
	RawQuery   string `sql:"type:varchar(256)"`
	RawData    string
	UserAgent  string
	Headers    string
	IP         string `sql:"type:varchar(46)"`
	Method     string `sql:"type:varchar(7)"`
	Path       string `sql:"type:varchar(255)"`
	StatusCode int
	Latency    int64
}
