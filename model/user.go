package model

import (
	"time"

	"github.com/jinzhu/gorm"
)

type User struct {
	ID        uint `gorm:"primary_key"`
	CreatedAt time.Time
	UpdatedAt time.Time  `json:"-"`
	DeletedAt *time.Time `sql:"index" json:"-"`
	Name      string
	Username  string
	Password  string
	Level     int
	AvatarURL string
}

func (User) AddIndex(db *gorm.DB) error {
	return db.Model(User{}).AddUniqueIndex("idx_user_and_password", "username", "password").Error
}
