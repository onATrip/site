package model

import (
	"time"
)

//go:generate enumer -type=RouteType -transform=snake -trimprefix=RouteType -sql -text
type RouteType int

const (
	RouteTypeNone RouteType = iota
	RouteTypeBicycle 
	RouteTypeBoat
	RouteTypeCar
	RouteTypeFly
	RouteTypeHiking
	RouteTypeMuslim
	RouteTypeOrthodox
	RouteTypeRun
	RouteTypeWalk
)

type Route struct {
	ID         uint `gorm:"primary_key"`
	CreatedAt  time.Time
	UpdatedAt  time.Time  `json:"-"`
	DeletedAt  *time.Time `sql:"index" json:"-"`
	RouteType  RouteType  `sql:"type:route_type"`
	Title      string
	UserID     uint64 `json:"-"`
	User       User
	PreviewURL string
	Duration   time.Duration
	Distance   float64
	Points     []Point
	Comments   []Comment
}
