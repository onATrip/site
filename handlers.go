package main

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"net/http"
	"regexp"
	"strings"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
	"github.com/pkg/errors"
	"gitlab.com/onATrip/site.git/model"
	"go.uber.org/zap"
)

func httpLogger(db *gorm.DB, l *zap.Logger) gin.HandlerFunc {
	return func(c *gin.Context) {
		path := c.Request.URL.Path

		if path == "/status" || path == "/info" {
			return
		}

		if strings.HasPrefix(path, "/static") {
			return
		}

		rawQuery := c.Request.URL.RawQuery

		// Start timer
		start := time.Now()

		rawData, _ := c.GetRawData()
		c.Request.Body = ioutil.NopCloser(bytes.NewReader(rawData))

		// before request
		c.Next()
		// after request

		// Stop timer
		latency := time.Since(start).Nanoseconds()

		statusCode := c.Writer.Status()

		if statusCode == http.StatusNotFound {
			return
		}

		ua := c.GetHeader("User-Agent")
		// Remove other symbols
		filter := regexp.MustCompile(`[^\w\d\s().,;/\\]+`)
		ua = filter.ReplaceAllString(ua, "")

		clientIP := c.ClientIP()
		method := c.Request.Method
		headers := fmt.Sprintf("%+v", c.Request.Header)

		// log to db
		if err := db.Create(&model.Visit{
			IP:         clientIP,
			RawQuery:   rawQuery,
			RawData:    string(rawData),
			UserAgent:  ua,
			Latency:    latency,
			Path:       path,
			StatusCode: statusCode,
			Method:     method,
			Headers:    headers,
		}).Error; err != nil {
			l.Error("can't log in middleware", zap.Error(err))
		}
	}
}

func loginHandler(c *gin.Context) {
	c.HTML(http.StatusOK, "login", nil)
}

func indexHandler(db *gorm.DB, l *zap.Logger) gin.HandlerFunc {
	return func(c *gin.Context) {
		authData, err := getAuthData(c, db)
		if err != nil {
			l.Error("can't get auth data", zap.Error(err))
		}

		c.HTML(http.StatusOK, "site/index", Site{AuthData: authData})
	}
}

func routeHandler(db *gorm.DB, l *zap.Logger) gin.HandlerFunc {
	return func(c *gin.Context) {
		var routes []model.Route

		if err := db.Preload("User").Order("id desc").Limit(50).Find(&routes).Error; err != nil {
			l.Error("can't find routes", zap.Error(err))
		}

		authData, err := getAuthData(c, db)
		if err != nil {
			l.Error("can't get auth data", zap.Error(err))
		}

		c.HTML(http.StatusOK, "route/index", Site{Routes: routes, AuthData: authData, Date: "пн, 15.10"})
	}
}

func getAuthData(c *gin.Context, db *gorm.DB) (model.User, error) {
	var authData model.User
	if cookie, err := c.Cookie(authCookieName); err == nil {
		data := strings.Split(cookie, cookieSeporator)
		if len(data) == 2 {
			username := data[0]
			password := data[1]

			err := db.Where("username = ?", username).
				Where("password = ?", password).
				First(&authData).Error
			if err != nil && err != gorm.ErrRecordNotFound {
				return model.User{}, errors.Wrap(err, "can't find user")
			}
		}
	}

	return authData, nil
}
