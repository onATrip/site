package main

import (
	"gitlab.com/onATrip/site.git/model"
)

func avgRating(comments []model.Comment) int {
	if len(comments) == 0 {
		return 0
	}

	var score, i int
	for _, comment := range comments {
		score += comment.Rating
		i++
	}

	return score / i
}
