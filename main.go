package main

import (
	"html/template"
	"log"

	"github.com/gin-gonic/gin"
	"go.uber.org/zap"
)

func main() {
	cfg, err := initConfig()
	if err != nil {
		log.Fatalf("can't init config: %s", err)
	}

	// Init log
	var l *zap.Logger
	if cfg.Debug {
		l, err = zap.NewDevelopment()
	} else {
		l, err = zap.NewProduction()
	}
	if err != nil {
		log.Fatalf("can't initialize zap logger: %v", err)
	}

	defer func() {
		err = l.Sync()
		if err != nil {
			l.Error("can't zap sync", zap.Error(err))
		}
	}()

	l.Debug("zap init", zap.Any("config", cfg))

	db, err := cfg.DB.initDB(l)
	if err != nil {
		l.Fatal("db open error", zap.Error(err))
	}
	defer func() {
		err = db.Close()
		if err != nil {
			l.Error("db close error", zap.Error(err))
		}
	}()

	if err := initDBMigration(db); err != nil {
		l.Fatal("can't init db migration", zap.Error(err))
	}

	r := gin.New()
	r.Use(httpLogger(db, l))
	r.Use(gin.Recovery())

	r.Static("/static", "./static")

	r.SetFuncMap(template.FuncMap{
		"avgRating": avgRating,
	})
	r.LoadHTMLGlob("templates/*/*")

	r.GET("/.", indexHandler(db, l))
	r.GET("/route", routeHandler(db, l))
	r.GET("/login", loginHandler)

	adminGroup := r.Group("/admin")

	adminGroup.GET("/createRoute", adminCreateRouteHandler(db, l))
	adminGroup.GET("/createPoint", adminCreatePointHandler(db, l))
	adminGroup.GET("/createUser", adminCreateUserHandler(db, l))

	apiGroup := r.Group("/api")
	apiGroup.GET("/getRoute", apiGetRoute(db, l))
	apiGroup.POST("/createRoute", adminAPICreateRouteHandler(db, l))
	apiGroup.POST("/createPoint", adminAPICreatePointHandler(db, l))
	apiGroup.POST("/createUser", adminAPICreateUserHandler(db, l))
	apiGroup.POST("/login", apiLogInHandler(db, l))
	apiGroup.POST("/signup", apiSignUpHandler(db, l))

	if err := r.Run(":8080"); err != nil {
		log.Fatalln("can't run server", err)
	}
}
