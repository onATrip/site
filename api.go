package main

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
	"gitlab.com/onATrip/site.git/model"
	"go.uber.org/zap"
)

func apiGetRoute(db *gorm.DB, l *zap.Logger) gin.HandlerFunc {
	return func(c *gin.Context) {
		id := c.Query("id")
		if id == "" {
			c.JSON(http.StatusBadRequest, gin.H{"error": "id is empty"})
			return
		}

		var route model.Route
		if err := db.Set("gorm:auto_preload", true).First(&route, id).Error; err != nil {
			l.Error("can't get route", zap.Error(err), zap.String("id", id))
			c.JSON(http.StatusBadRequest, gin.H{"error": "trip not exist"})
			return
		}

		c.JSON(http.StatusOK, route)
	}
}

type apiCreateCommentParams struct {
	Text   string `form:"text"`
	Rating int    `form:"rating"`
}

func apiCreateCommentHandler(db *gorm.DB, l *zap.Logger) gin.HandlerFunc {
	return func(c *gin.Context) {

	}
}

const maxAge = 60 * 60 * 24 * 12 * 10

const authCookieName = "usernameAndPassword"
const cookieSeporator = "|||"

type apiLogInParameters struct {
	Username string `form:"username" binding:"required"`
	Password string `form:"password" binding:"required"`
}

func apiLogInHandler(db *gorm.DB, l *zap.Logger) gin.HandlerFunc {
	return func(c *gin.Context) {
		var p apiLogInParameters
		if err := c.Bind(&p); err != nil {
			l.Warn("can't parse log in params", zap.Error(err))
			c.JSON(http.StatusBadRequest, gin.H{"error": "missing params"})
			return
		}

		var user model.User
		err := db.Where("username = ?", p.Username).Where("password = ?", p.Password).First(&user).Error
		if err != nil && err == gorm.ErrRecordNotFound {
			l.Warn("user not found", zap.Error(err), zap.String("username", p.Username), zap.String("password", p.Password))
			c.JSON(http.StatusBadRequest, gin.H{"error": "user not found"})
			return
		}

		c.SetCookie(authCookieName, user.Username+cookieSeporator+user.Password, maxAge, "", "", false, true)

		c.JSON(http.StatusOK, gin.H{
			"success": true,
		})
	}
}

type apiSingUpParameters struct {
	Name      string `form:"name" binding:"required"`
	AvatarURL string `form:"avatarURL" binding:"required"`
	Username  string `form:"username" binding:"required"`
	Password  string `form:"password" binding:"required"`
}

func apiSignUpHandler(db *gorm.DB, l *zap.Logger) gin.HandlerFunc {
	return func(c *gin.Context) {
		var p apiSingUpParameters
		if err := c.Bind(&p); err != nil {
			l.Warn("can't parse sing up params", zap.Error(err))
			c.JSON(http.StatusBadRequest, gin.H{"error": "missing params"})
			return
		}

		table := model.User{
			Name:      p.Name,
			AvatarURL: p.AvatarURL,
			Username:  p.Username,
			Password:  p.Password,
		}
		if err := db.Create(&table).Error; err != nil {
			l.Error("can't create user", zap.Error(err))
			c.JSON(http.StatusBadRequest, gin.H{"error": "can't create user"})
			return
		}

		c.SetCookie(authCookieName, table.Username+cookieSeporator+table.Password, maxAge, "", "", false, true)

		c.JSON(http.StatusOK, gin.H{
			"success": true,
		})
	}
}
