package main

import (
	"fmt"

	"github.com/golang-migrate/migrate"
	"github.com/golang-migrate/migrate/database/postgres"
	_ "github.com/golang-migrate/migrate/source/file"
	"github.com/hypnoglow/gormzap"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	_ "github.com/lib/pq"
	"github.com/pkg/errors"
	"gitlab.com/onATrip/site.git/model"
	"go.uber.org/zap"
)

// DBConfig is config for db Postgre
type DBConfig struct {
	Host        string `env:"DB_HOST" required:"true"`
	Name        string `env:"DB_NAME" required:"true"`
	User        string `env:"DB_USER" required:"true"`
	Password    string `env:"DB_PASSWORD" required:"true"`
	Logging     bool   `env:"DB_DEBUG" default:"false"`
	MaxOpenConn int    `env:"DB_MAX_OPEN_CONN" default:"50"`
	MaxIdleConn int    `env:"DB_MAX_IDLE_CONN" default:"10"`
}

func (c DBConfig) makeDBInfo() string {
	return fmt.Sprintf("host=%s user=%s dbname=%s sslmode=disable password=%s",
		c.Host, c.User, c.Name, c.Password,
	)
}

func (c DBConfig) initDB(l *zap.Logger) (*gorm.DB, error) {
	// Init PG connect
	db, err := gorm.Open("postgres", c.makeDBInfo())
	if err != nil {
		return nil, errors.Wrap(err, "can't open db connection")
	}

	// Set log db mode
	db.LogMode(c.Logging)

	db.SetLogger(gormzap.New(l, gormzap.WithLevel(zap.InfoLevel)))

	// Set max pool
	db.DB().SetMaxIdleConns(c.MaxIdleConn)
	db.DB().SetMaxOpenConns(c.MaxOpenConn)

	return db, err
}

const tablePrefix = "oat_"

func initDBMigration(db *gorm.DB) error {
	driver, err := postgres.WithInstance(db.DB(), &postgres.Config{})
	if err != nil {
		return errors.Wrap(err, "can't init migration with instance")
	}
	m, err := migrate.NewWithDatabaseInstance(
		"file://src/migrations",
		"postgres", driver)

	if err != nil {
		return errors.Wrap(err, "cant init new db instance")
	}

	if err := m.Up(); err != nil && err != migrate.ErrNoChange {
		return errors.Wrap(err, "can't upgrade")
	}

	// Add tables prefixes.
	gorm.DefaultTableNameHandler = func(db *gorm.DB, defaultTableName string) string {
		return tablePrefix + defaultTableName
	}

	// Init auto-migrate.
	if err := db.AutoMigrate(
		&model.User{},
		&model.Comment{},
		&model.Route{},
		&model.Point{},
		&model.Visit{},
	).Error; err != nil {
		return errors.Wrap(err, "can't init migration")
	}

	if err := model.User.AddIndex(model.User{}, db); err != nil {
		return errors.Wrap(err, "can't create unique index")
	}

	return nil
}
