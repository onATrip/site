package main

import (
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
	"gitlab.com/onATrip/site.git/model"
	"go.uber.org/zap"
)

type adminAPICreateRouteParams struct {
	Title      string          `form:"title" binding:"required"`
	RouteType  model.RouteType `form:"routeType"`
	UserID     uint64          `form:"userID" binding:"required"`
	PreviewURL string          `form:"previewURL" binding:"required"`
	Duration   time.Duration   `form:"duration" binding:"required"`
	Distance   float64         `form:"distance" binding:"required"`
}

func adminAPICreateRouteHandler(db *gorm.DB, l *zap.Logger) gin.HandlerFunc {
	return func(c *gin.Context) {
		var p adminAPICreateRouteParams
		if err := c.Bind(&p); err != nil {
			l.Warn("can't parse params", zap.Error(err))
			c.JSON(http.StatusBadRequest, gin.H{"error": err})
			return
		}

		table := model.Route{
			Title:      p.Title,
			RouteType:  p.RouteType,
			UserID:     p.UserID,
			PreviewURL: p.PreviewURL,
			Duration:   p.Duration * time.Minute,
			Distance:   p.Distance,
		}

		if err := db.Create(&table).Error; err != nil {
			l.Error("can't create route table", zap.Error(err), zap.Any("table", table))
			c.Status(http.StatusInternalServerError)
			return
		}

		c.JSON(http.StatusOK, gin.H{
			"success": true,
			"id":      table.ID,
		})
	}
}

type adminAPICreatePointParams struct {
	RouteID     uint64  `form:"routeID" binding:"required"`
	Title       string  `form:"title" binding:"required"`
	ImageURL    string  `form:"imageURL" binding:"required"`
	Latitude    float64 `form:"lat" binding:"required"`
	Longitude   float64 `form:"Longitude" binding:"required"`
	Description string  `form:"description" binding:"required"`
	Rating      float64 `form:"rating" binding:"required"`
}

func adminAPICreatePointHandler(db *gorm.DB, l *zap.Logger) gin.HandlerFunc {
	return func(c *gin.Context) {
		var p adminAPICreatePointParams
		if err := c.Bind(&p); err != nil {
			l.Warn("can't parse params", zap.Error(err))
			c.JSON(http.StatusBadRequest, gin.H{"error": err})
			return
		}

		table := model.Point{
			RouteID:     p.RouteID,
			Title:       p.Title,
			ImageURL:    p.ImageURL,
			Latitude:    p.Latitude,
			Longitude:   p.Longitude,
			Description: p.Description,
			Rating:      p.Rating,
		}

		if err := db.Create(&table).Error; err != nil {
			l.Error("can't create point table", zap.Error(err), zap.Any("table", table))
			c.Status(http.StatusInternalServerError)
			return
		}

		c.JSON(http.StatusOK, gin.H{
			"success": true,
			"id":      table.ID,
		})
	}
}

type adminAPICreateUserParams struct {
	Name      string `form:"name"`
	Level     int    `form:"level"`
	AvatarURL string `form:"avatarURL"`
}

func adminAPICreateUserHandler(db *gorm.DB, l *zap.Logger) gin.HandlerFunc {
	return func(c *gin.Context) {
		var p adminAPICreateUserParams
		if err := c.Bind(&p); err != nil {
			l.Warn("can't parse params", zap.Error(err))
			c.JSON(http.StatusBadRequest, gin.H{"error": err})
			return
		}

		table := model.User{
			Name:      p.Name,
			Level:     p.Level,
			AvatarURL: p.AvatarURL,
		}

		if err := db.Create(&table).Error; err != nil {
			l.Error("can't create user table", zap.Error(err), zap.Any("table", table))
			c.Status(http.StatusInternalServerError)
			return
		}

		c.JSON(http.StatusOK, gin.H{
			"success": true,
			"id":      table.ID,
		})
	}
}
