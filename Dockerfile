FROM scratch
COPY static /static
COPY templates /templates
COPY src/migrations /src/migrations
COPY srv /
EXPOSE 8080
ENTRYPOINT ["/srv"]
