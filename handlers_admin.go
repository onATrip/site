package main

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
	"gitlab.com/onATrip/site.git/model"
	"go.uber.org/zap"
)

func adminCreateRouteHandler(db *gorm.DB, l *zap.Logger) gin.HandlerFunc {
	return func(c *gin.Context) {

		data := gin.H{
			"RouteTypes": model.RouteTypeValues(),
		}
		c.HTML(http.StatusOK, "admin/createRoute", data)
	}
}

func adminCreatePointHandler(db *gorm.DB, l *zap.Logger) gin.HandlerFunc {
	return func(c *gin.Context) {
		c.HTML(http.StatusOK, "admin/createPoint", nil)
	}
}

func adminCreateUserHandler(db *gorm.DB, l *zap.Logger) gin.HandlerFunc {
	return func(c *gin.Context) {
		c.HTML(http.StatusOK, "admin/createUser", nil)
	}
}
