package main

import (
	"github.com/jinzhu/configor"
)

type Config struct {
	Debug    bool   `env:"DEBUG_MODE"`
	Timezone string `env:"TIMEZONE" required:"true"`
	DB       DBConfig
}

func initConfig() (*Config, error) {
	cfg := &Config{}

	if err := configor.New(&configor.Config{ENVPrefix: ""}).Load(cfg); err != nil {
		return nil, err
	}

	return cfg, nil
}
