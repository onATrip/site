package main

import (
	"gitlab.com/onATrip/site.git/model"
)

type Site struct {
	AuthData model.User
	Routes   []model.Route
	Date string
}

/* var siteData = Site{
	Routes: []model.Route{
		{
			ID:        1,
			RouteType: RouteTypeBicycle,
			Title:     "Крутое велокольцо",
			Preview:   "https://trello-attachments.s3.amazonaws.com/5bc24216af3ab2317d99f318/5bc242824dfdb984a2b6ad85/bff0482c465937c3e303be4f26be6979/Screenshot_from_2018-10-13_20-12-15.png",
			Rating:    4.2,
			User: User{
				Name:   "Maksim S.",
				Level:  80,
				Avatar: "https://assets.gitlab-static.net/uploads/-/system/user/avatar/479991/avatar.png?width=10",
			},
			Duration: time.Hour * 2,
			Distance: 12.5,
			Points: []Point{
				{
					ID:          1,
					Title:       "Храм василия блаженного",
					Lat:         55.752732,
					Longitude:         37.622710,
					Description: "",
					Rating:      4.4,
					Photo:       "",
				},
				{
					ID:          2,
					Title:       "Парк Зарядье",
					Lat:         55.751231,
					Longitude:         37.629181,
					Description: "",
					Rating:      4.4,
					Photo:       "",
				},
				{
					ID:          3,
					Title:       "Георгиевская церковь",
					Lat:         55.752573,
					Longitude:         37.630560,
					Description: "",
					Rating:      4.4,
					Photo:       "",
				},
			},
		},
		{
			ID:        2,
			RouteType: RouteTypeBicycle,
			Title:     "Крутое велокольцо",
			Preview:   "https://trello-attachments.s3.amazonaws.com/5bc24216af3ab2317d99f318/5bc242824dfdb984a2b6ad85/bff0482c465937c3e303be4f26be6979/Screenshot_from_2018-10-13_20-12-15.png",
			Rating:    4.2,
			User: User{
				Name:   "Maksim S.",
				Level:  80,
				Avatar: "https://assets.gitlab-static.net/uploads/-/system/user/avatar/479991/avatar.png?width=10",
			},
			Duration: time.Hour * 2,
			Distance: 12.5,
			Points:   []Point{},
		},
		{
			ID:        3,
			RouteType: RouteTypeBicycle,
			Title:     "Крутое велокольцо",
			Preview:   "https://trello-attachments.s3.amazonaws.com/5bc24216af3ab2317d99f318/5bc242824dfdb984a2b6ad85/bff0482c465937c3e303be4f26be6979/Screenshot_from_2018-10-13_20-12-15.png",
			Rating:    4.2,
			User: User{
				Name:   "Maksim S.",
				Level:  80,
				Avatar: "https://assets.gitlab-static.net/uploads/-/system/user/avatar/479991/avatar.png?width=10",
			},
			Duration: time.Hour * 2,
			Distance: 12.5,
			Points:   []Point{},
		},
		{
			ID:        4,
			RouteType: RouteTypeBicycle,
			Title:     "Крутое велокольцо",
			Preview:   "https://trello-attachments.s3.amazonaws.com/5bc24216af3ab2317d99f318/5bc242824dfdb984a2b6ad85/bff0482c465937c3e303be4f26be6979/Screenshot_from_2018-10-13_20-12-15.png",
			Rating:    4.2,
			User: User{
				Name:   "Maksim S.",
				Level:  80,
				Avatar: "https://assets.gitlab-static.net/uploads/-/system/user/avatar/479991/avatar.png?width=10",
			},
			Duration: time.Hour * 2,
			Distance: 12.5,
			Points:   []Point{},
		},
	},
} */
